package lv.andrejsproscenko.lesson1;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;


import java.util.Random;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RelativeLayout mainLayout = findViewById(R.id.mainLayout);
        mainLayout.setBackgroundColor(this.getRandomInt());
    }

    @Override
    protected void onPause() {
        super.onPause();
        RelativeLayout mainLayout = findViewById(R.id.mainLayout);
        mainLayout.setBackgroundColor(Color.GREEN);

    }

    public void onButtonPress(View view) {
        Button myButton = findViewById(R.id.myButton);
        myButton.setBackgroundColor(this.getRandomInt());

        int backgroundColor = this.getRandomInt();

        RelativeLayout mainLayout = findViewById(R.id.mainLayout);
        mainLayout.setBackgroundColor(backgroundColor);

        TextView myText = findViewById(R.id.myText);
        myText.setText("Background color: " + backgroundColor);



    }

    private int getRandomInt (){
        Random random = new Random();
        int randomColor = random.nextInt();
        System.out.println("Random color: " + randomColor);
        return randomColor;
    }
}
